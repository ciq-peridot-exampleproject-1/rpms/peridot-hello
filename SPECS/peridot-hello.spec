Name:           peridot-hello
Version:        1.0.0
Release:        1%{?dist}
Summary:        Example Peridot src-git projecft
License:        BSD-3
Source0:        https://www.example.com/%{name}/releases/%{name}-%{version}.tar.gz
BuildArch:      noarch

%description
An example bash script that only prints "Hello World!" for the purpose of testing
Peridot standalone projects

%prep
%setup -q

%build

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 0755 %{name} %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}